package fr.tl.ilog.jface;

import java.io.File;
import java.io.Writer;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Tree;

public class FileExplorer extends ApplicationWindow implements ISelectionChangedListener, IDoubleClickListener {
	protected Action actExit;
	protected TableViewer tbvw;
	protected OpenAction actOpen;
	protected FileContentProvider cp;
	protected File filScripts;
	private SashForm sashConsole;
	private Control sashExplorer;
	private StyledText console;
	private Color colBlue;
	private Color colRed;
	protected ScriptEngine js;
	private Action[] actaMenus;
	private Action[] actaPopups;
	private MenuManager mmBar;

	/**
	 * Create the application window.
	 */
	public FileExplorer() {
		super(null);
		filScripts = new File("scripts");
		checkScripting();
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
	}

	protected void createPopupMenu() {
		MenuManager mmCtx = new MenuManager();
		mmCtx.add(actOpen);
		Table table = tbvw.getTable();
		Menu mnCtx = mmCtx.createContextMenu(table);
		table.setMenu(mnCtx);
	}

	protected Control createSashFormExplorer(Composite parent) {
		SashForm sashFormExplorer = new SashForm(parent, SWT.NONE);
		createTreeViewer(sashFormExplorer);
		createTableViewer(sashFormExplorer);
		sashFormExplorer.setWeights(new int[] { 1, 3 });
		return sashFormExplorer;
	}

	protected void createTableViewer(SashForm sashForm) {
		tbvw = new TableViewer(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
		Table table = tbvw.getTable();
		table.setHeaderVisible(true);
		TableColumn tcName = new TableColumn(table, SWT.LEFT);
		tcName.setText("Name");
		tcName.setWidth(300);
		TableColumn tcSize = new TableColumn(table, SWT.RIGHT);
		tcSize.setText("Size");
		tcSize.setWidth(150);
		FileContentProvider cp = new FileContentProvider();
		tbvw.setContentProvider(cp);
		tbvw.setLabelProvider(cp);
		TableSorter sorter = new TableSorter();
		tbvw.setSorter(sorter);
		tbvw.addDoubleClickListener(this);
	}

	protected void createTreeViewer(SashForm sashForm) {
		TreeViewer trvw = new TreeViewer(sashForm, SWT.BORDER);
		trvw.addSelectionChangedListener(this);
		Tree tree = trvw.getTree();
		FileContentProvider cp = getContentProvider();
		trvw.setContentProvider(cp);
		trvw.setLabelProvider(cp);
		ViewerFilter filter = new TreeFilter();
		trvw.setFilters(new ViewerFilter[] { filter });
		trvw.setInput(new Root());
	}

	protected FileContentProvider getContentProvider() {
		if (cp == null)
			cp = new FileContentProvider();
		return cp;
	}

	/**
	 * Create the actions.
	 */
	protected void createActions() {
		actExit = new ExitAction(this);
		actOpen = new OpenAction(this);
	}

	/**
	 * Create the menu manager.
	 * 
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		mmBar = new MenuManager("menu");
		MenuManager mmFile = new MenuManager("&File");
		mmBar.add(mmFile);
		mmFile.add(actExit);
		return mmBar;
	}


	/**
	 * Create the status line manager.
	 * 
	 * @return the status line manager
	 */
	@Override
	protected StatusLineManager createStatusLineManager() {
		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			FileExplorer window = new FileExplorer();
			window.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void run() {
		setBlockOnOpen(true);
		open();
		Display.getCurrent().dispose();
	}

	/**
	 * Configure the shell.
	 * 
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("File Explorer");
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(600, 400);
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		ISelection sel = event.getSelection();
		if (sel instanceof TreeSelection) {
			TreeSelection tsel = (TreeSelection) sel;
			Object elt = tsel.getFirstElement();
			tbvw.setInput(elt);
		}
	}

	public TableViewer getTableViewer() {
		return tbvw;
	}

	@Override
	public void doubleClick(DoubleClickEvent e) {
		ISelection sel = e.getSelection(); // s�lection de la table
		if (sel instanceof StructuredSelection) {
			StructuredSelection ssel = (StructuredSelection) sel;
			Object elt = ssel.getFirstElement();
			if (elt instanceof File) {
				File file = (File) elt;
				if (file.isDirectory())
					tbvw.setInput(elt);
				else
					actOpen.run();
			}
		}
	}

	@Override
	protected Control createContents(Composite parent) {
		createSashFormConsole(parent);
		createMenuActions();
		createMenu();
		createPopupMenu();
		setStatus("Ready.");
		return sashExplorer;
	}

	protected void createSashFormConsole(Composite parent) {
		sashConsole = new SashForm(parent, SWT.VERTICAL);
		sashExplorer = createSashFormExplorer(sashConsole);
		createConsole(sashConsole);
		checkScripting();
		if (js == null) {
			sashConsole.setWeights(new int[] { 3, 0 });
		} else {
			sashConsole.setWeights(new int[] { 3, 1 });
		}
	}

	protected void createConsole(Composite parent) {
		console = new StyledText(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		Display display = Display.getCurrent();
		colBlue = display.getSystemColor(SWT.COLOR_BLUE);
		colRed = display.getSystemColor(SWT.COLOR_RED);
	}

	protected void append(String msg, Color col) {
		msg = msg + '\n';
		console.append(msg);
		StyleRange style = new StyleRange();
		style.foreground = col;
		style.length = msg.length();
		style.start = console.getText().length() - style.length;
		console.setStyleRange(style);
	}

	public void clear() {
		console.setText("");
	}

	public void out(String msg) {
		append(msg, colBlue);
	}

	public void err(String msg) {
		append(msg, colRed);
	}

	public void checkScripting() {
		File f = new File("scripts");
		if (f.exists() && f.isDirectory()) {
			ScriptEngineManager factory = new ScriptEngineManager();
			js = factory.getEngineByName("JavaScript");
			js.getContext().setWriter(new ConsoleWriter(this));
		}
	}

	protected Action[] createActions(String namDir) {
		// pour créer les actions du dossier scripts/<namDir>
		File scriptsDir = new File(filScripts.getAbsolutePath() + "/" + namDir);
		if (scriptsDir.exists()) {
			Action[] actions = new Action[scriptsDir.listFiles().length];
			int i = 0;
			for (File jsScript : scriptsDir.listFiles()) {
				actions[i] = new JsAction(this, jsScript);
				i++;
			}
			return actions;
		}
		
		return null;

	}

	protected void createMenuActions() {
		if (js != null) {
			actaMenus = createActions("menus");
			actaPopups = createActions("contextMenus");
		}
	}

	protected void createMenu() {
		// pour créer les menus liés au scripting
		MenuManager mmMenu = new MenuManager("&Menu");
		mmBar.add(mmMenu);
		for(Action action : actaMenus){
			mmMenu.add(action);
		}
		mmBar.update(true);
	}
}
