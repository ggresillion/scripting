package fr.tl.ilog.jface;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

public class Autre extends ApplicationWindow {

	/**
	 * Create the application window.
	 */
	public Autre() {
		super(null);
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
	}

	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Tree tree = new Tree(container, SWT.BORDER);
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		
		TreeColumn trclmnName = new TreeColumn(tree, SWT.NONE);
		trclmnName.setWidth(115);
		trclmnName.setText("Name");
		
		TreeColumn trclmnSize = new TreeColumn(tree, SWT.NONE);
		trclmnSize.setWidth(100);
		trclmnSize.setText("Rank");
		
		TreeItem trtmA = new TreeItem(tree, SWT.NONE);
		trtmA.setText(0, "A");
		trtmA.setText(1, "1");
		
		TreeItem trtmAa = new TreeItem(trtmA, SWT.NONE);
		trtmAa.setText(0, "AA");
		trtmAa.setText(1, "1.1");
		
		TreeItem trtmAaa = new TreeItem(trtmAa, SWT.NONE);
		trtmAaa.setText(0, "AAA");
		trtmAaa.setText(1, "1.1.1");
		trtmAa.setExpanded(true);
		
		TreeItem trtmAb = new TreeItem(trtmA, SWT.NONE);
		trtmAb.setText(0, "AB");
		trtmAb.setText(1, "1.2");
		trtmA.setExpanded(true);
		
		TreeItem trtmB = new TreeItem(tree, SWT.NONE);
		trtmB.setText(0, "B");
		trtmB.setText(1, "2");
		
		TreeItem trtmBb = new TreeItem(trtmB, SWT.NONE);
		trtmBb.setText(0, "BA");
		trtmBb.setText(1, "2.1");
		trtmB.setExpanded(true);
		
		TreeItem trtmC = new TreeItem(tree, SWT.NONE);
		trtmC.setText(0, "C");
		trtmC.setText(1, "3");
		
		TreeItem trtmCa = new TreeItem(trtmC, SWT.NONE);
		trtmCa.setText(0, "CA");
		trtmCa.setText(1, "3.1");
		trtmC.setExpanded(true);

		return container;
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Create the menu manager.
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager("menu");
		return menuManager;
	}

	/**
	 * Create the toolbar manager.
	 * @return the toolbar manager
	 */
	@Override
	protected ToolBarManager createToolBarManager(int style) {
		ToolBarManager toolBarManager = new ToolBarManager(style);
		return toolBarManager;
	}

	/**
	 * Create the status line manager.
	 * @return the status line manager
	 */
	@Override
	protected StatusLineManager createStatusLineManager() {
		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Autre window = new Autre();
			window.setBlockOnOpen(true);
			window.open();
			Display.getCurrent().dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("New Application");
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(700, 500);
	}
}
