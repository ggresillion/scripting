package fr.tl.ilog.jface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.script.ScriptException;

import org.eclipse.jface.action.Action;

public class JsAction extends Action {
	protected FileExplorer explo;
	protected File script;

	public JsAction(FileExplorer fileExplorer, File file) {
		explo = fileExplorer;
		script = file;
		setText(file.getName());
		setToolTipText("Execute " + file.getName());
	}

	@Override
	public void run() {
		try {
			explo.js.eval(new FileReader(script));
		} catch (FileNotFoundException | ScriptException e) {
			explo.err(e.getMessage());
		}
	}
}
