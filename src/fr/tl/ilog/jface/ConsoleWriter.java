package fr.tl.ilog.jface;

import org.eclipse.swt.graphics.Color;
import java.io.IOException;
import java.io.Writer;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.widgets.Display;

public class ConsoleWriter extends Writer{
	
	FileExplorer explo;
	String temp = "";
	
	public ConsoleWriter(FileExplorer fileExplorer){
		explo = fileExplorer;
	}

	@Override
	public void close() throws IOException {
		
	}

	@Override
	public void flush() throws IOException {
		if (!temp.equals("")){
			Device device = Display.getCurrent ();
			Color blue = new Color (device, 0, 0, 255);
			explo.append(temp, blue);
			temp = "";
		}
	}

	@Override
	public void write(char[] arg0, int arg1, int arg2) throws IOException {
		for ( int i = arg1; i < arg2 ; i ++){
			temp = temp + arg0[i];
		}
	}

}
